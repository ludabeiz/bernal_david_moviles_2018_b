package com.example.davidbernal.otraformaexamen

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.TextView;
import android.widget.Button;
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    internal lateinit var startButton: Button;
    internal lateinit var tapButton: Button;
    internal lateinit var ramdontext: TextView;
    internal lateinit var puntajetext: TextView;


    internal var tiempoRestante = 10;
    internal var puntaje = 0;
    internal var numeroRandom = -1;
    internal var juegoIniciado = false;

    //variables para el timeleft //cuando son variables en kotlin se pone Var cuando son constantes se pone val

    internal lateinit var countDownTimer: CountDownTimer;
    internal val countDownInterval = 1000L;
    internal val initialCountDown = 10000L;


    internal val TAG = MainActivity::class.java.simpleName;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //connect view to variables
        puntajetext = findViewById<TextView>(R.id.puntaje_text);
        startButton = findViewById<Button>(R.id.start_Button);
        tapButton = findViewById<Button>(R.id.tap_Button);
        ramdontext = findViewById<TextView>(R.id.ramdon_text);

        startButton.setOnClickListener{ _ -> startGame()};

        tapButton.setOnClickListener{ _ -> pararTiempo()};
        resetGame();
    }

    private fun pararTiempo(){
        if(numeroRandom == tiempoRestante){
            puntaje += 100;
        }else if (numeroRandom >= tiempoRestante-1 && numeroRandom <= tiempoRestante+1){
            puntaje += 50;
        }else{
            puntaje += 0;
        }
        puntajetext.text = getString(R.string.puntaje_text, puntaje.toString());


        countDownTimer.cancel();//para que el contador se vuevla en cero
        endGame();
    }

    private fun iniciarContador(){
        countDownTimer = object : CountDownTimer(initialCountDown, countDownInterval){
            override fun onTick(millisUntilFinished: Long) {
                tiempoRestante = millisUntilFinished.toInt() / 1000;
            }

            override fun onFinish() {
                endGame();
            }
        }
    }

    private fun resetGame(){
        tiempoRestante = 10;
        numeroRandom =-1;
        ramdontext.text = "Número Aleatorio"
        juegoIniciado = false;

    }

    private fun startGame(){
        iniciarContador();

            countDownTimer.start();
            numeroRandom = (0.. 10).shuffled().first();
            ramdontext.text= getString(R.string.random_text, numeroRandom.toString());
            juegoIniciado = true;

    }
    private fun endGame(){
        if(juegoIniciado) {
            resetGame();
        }

    }
}
